package br.sp.senai.informatica.ozcorps;

public class TesteFuncionario {

	public static void main(String[] args) {
		Funcionario p = new Funcionario ("Rodney","14564646", "158679543", "123", "rodinyzinho@ozcorp.com", "DiretorIA", "852",TipoSanguineo.ABPOS , Sexo.MASCULINO, NivelAcesso.DIRETORIA);
		System.out.println("Funcionario: " + p.getNome());
		System.out.println("RG: " + p.getRg());
		System.out.println("CPF: " + p.getCpf());
		System.out.println("Matricula: " + p.getMatricula());
		System.out.println("E-mail: " + p.getEmail());
		System.out.println("Cargo: " + p.getCargo());
		System.out.println("Senha: " + p.getSenha());
		System.out.println("Tipo Sanguineo: " + p.getSangue().sangue);
		System.out.println("Sexo: " + p.getSexo().sexo);
		System.out.println("Nivel De Acesso: " + p.getAcesso().na);
		
	}

}
