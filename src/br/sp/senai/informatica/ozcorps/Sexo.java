package br.sp.senai.informatica.ozcorps;

public enum Sexo {
MASCULINO("Masculino"), FEMININO("Feminino"), OUTROS("Outro");
	
	public String sexo;
	
	Sexo(String sexo){
		this.sexo = sexo;
	}

}
