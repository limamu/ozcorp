package br.sp.senai.informatica.ozcorps;

public enum TipoSanguineo {
	APOS("A+"), ANEG("A-"), BPOS("B+"), BNEG("B-"), ABPOS("AB+"), ABNEG("AB-"), OPOS("O+"), ONEG("O-");
	
	public String sangue;
	
	TipoSanguineo(String sangue){
		this.sangue = sangue;
}
}
